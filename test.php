<?php

$directory = "./storage/app/locale/";
$domain = "domain";
$locale = "pt_PT.UTF-8";

putenv("LANGUAGE=".$locale);
putenv("LC_ALL=".$locale);
setlocale(LC_ALL, $locale);
setlocale(LC_MESSAGES, $locale);

var_dump(getenv("LC_ALL"));

var_dump(bindtextdomain($domain, $directory));
var_dump(textdomain($domain));

var_dump(gettext("Hi!"));
?>