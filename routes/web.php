<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'auth'], function() {
    /**
     * group them together..
     */
    Route::group(['prefix' => 'translations'], function() {
        Route::resource('/', "TranslationController");

        Route::get("/{id}", "TranslationController@show");

        Route::post('add', "TranslationController@add");
        Route::post('fork', "TranslationController@fork");
        Route::post('active', "TranslationController@active");
        Route::post('revised', "TranslationController@revised");
    });
});

Auth::routes();

Route::get('/', 'HomeController@index');
