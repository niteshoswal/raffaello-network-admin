@extends('layouts.app')

@section('content')
<div class="container">
    @if(count($errors) > 0)
        <ul class="alert alert-danger alert-dismissible list-inline" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    @endif
    {{ Form::open(['action' => 'TranslationController@store']) }}
    <h2>Create Translation</h2>
    <div class="form-group">
        {{ Form::label('key', 'Translation Key', ['class' => 'control-label']) }}
        {{ Form::text('key', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Enter reference translation key']) }}
    </div>
    @foreach($languages as $language => $name)
    <div class="form-group">
        {{ Form::label(sprintf('translation[%s]', $language), sprintf('Translation (%s)', $language)) }}
        {{ Form::textarea(sprintf('translation[%s]', $language), null, ['class' => 'form-control', 'placeholder' => sprintf('Enter %s translation', $name), 'rows' => 3] ) }}
    </div>
    @endforeach

    <div class="form-group">
        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
    </div>

    {{ Form::close() }}
</div>
@endsection