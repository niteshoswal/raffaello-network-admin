@extends("layouts.app")

@section('title', 'Translations')

@section('content')
<div class="container">
    <div class="row">
        {{ Form::open(["action" => 'TranslationController@index', 'method' => 'get', 'role' => 'form']) }}
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10">
            {{ Form::select("language", $languages, $selectedLanguage, ['class' => 'form-control']) }}
        </div>
        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
            {{ Form::submit("Switch", ['class' => 'btn btn-info']) }}
        </div>
        {{ Form::close() }}
        <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
        {{ link_to_action('TranslationController@create', 'Add New Translation', [], ['class' => 'btn btn-default'])}}
        {{ link_to_action('TranslationController@show', 'Build Locale', ["id" => $selectedLanguage], ['class' => 'btn btn-success']) }}
        </div>
    </div>
    <div class="row">
        @if(Session::has('flash_msg'))
        <div class="col-lg-12">
            <div class="alert alert-info" role="alert" style="margin-top: 20px;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ Session::get('flash_msg') }}
            </div>
        </div>
        @endif
        <div class="col-lg-12">
            <table class="table table-bordered table-hover table-translations">
                <caption><h2>All Translations</h2></caption>
                <thead>
                    <tr>
                        <th width="10%">Key</th>
                        <th width="22.5%">English Text</th>
                        <th width="22.5%">Translated Text</th>
                        <th class="text-center" width="5%">Revised</th>
                        <th class="text-center" width="5%">Auto</th>
                        <th width="10%">User</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($translations as $translation)
                        <tr>
                            <td class="bg-info">{{ $translation->key }}</td>
                            <td>{{ $translation->translation }}</td>
                            <td id="translation-{{$translation->id}}" data-id="{{$translation->id}}">
                                <button class="btn btn-link btn-add-translation" data-parent="#translation-{{$translation->id}}" data-key="{{ $translation->key }}" data-translated="{{ strlen($translation->translated_text) > 0 }}" data-type="{{ $selectedLanguage }}">
                                    {{ empty($translation->translated_text) ? "Add translation.." : $translation->translated_text }}
                                </button>
                            </td>
                            <td class="text-center"><i id="revised-{{$translation->id}}" class="fa fa-{{ ($translation->original_revised == '1' ? 'check-circle bg-success' : 'circle-o') }}"></i></td>
                            <td class="text-center"><i id="automatic-{{$translation->id}}" class="fa fa-{{ $translation->original_automatic == '1' ? 'check-circle bg-success' : 'circle-o' }}"></i></td>
                            <td id="translator-{{$translation->id}}">{{ $translation->translator }}</td>
                            <td class="text-center">
                                <button class="btn btn-default btn-fork" data-toggle="tooltip" data-placement="top" title="Revert" data-key="{{ $translation->key }}" data-type="{{ $selectedLanguage }}">
                                    <i class="fa fa-code-fork"></i>
                                </button>
                                <button style="{{ $translation->original_revised != '1' ? 'style: none;' : ''  }}" class="btn btn-default btn-revised" data-parent="#revised-{{$translation->id}}" data-toggle="tooltip" data-placement="top" title="Mark as revised" data-key="{{ $translation->key }}" data-type="{{ $selectedLanguage }}">
                                    <i class="fa fa-check-circle"></i>
                                </button>
                                <button style="{{ $translation->original_automatic != '1' ? 'style: none;' : ''  }}" class="btn btn-default btn-auto" data-toggle="tooltip" data-placement="top" title="Auto-Translate" data-key="{{ $translation->key }}" data-type="{{ $selectedLanguage }}">
                                    <i class="fa fa-magic"></i>
                                </button>
                                <!-- <button class="btn btn-success btn-save" data-toggle="tooltip" data-placement="top" title="Save" data-key="{{ $translation->key }}" data-type="{{ $translation->type }}">
                                    <i class="fa fa-floppy-o"></i>
                                </button> -->
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection