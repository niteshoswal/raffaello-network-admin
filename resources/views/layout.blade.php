<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
        <link rel="stylesheet" type="text/css" href="/components/bootstrap/dist/css/bootstrap.min.css" />
    </head>
    <body>
        <div class="container" style="margin-top: 60px;">
            @yield('content')
        </div>
        <script type="text/javascript" src="/components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>