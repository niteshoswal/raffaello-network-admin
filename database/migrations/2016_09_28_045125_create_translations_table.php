<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', [
                "english",
                "portugues",
                "espanol",
                "deutsch",
                "francais",
                "russian",
                "japanese",
                "chinese",
                "arabic"
            ]);
            $table->string('key');
            $table->text('translation');
            $table->enum('revised', ['0', '1']);
            $table->enum('automatic', ['0', '1']);

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->enum('active', ['0', '1']); //on a good day only 1 of the translation of a key will be active
            $table->timestamps();

            $table->index('key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translations');
    }
}
