<?php
namespace App\Support\Pomo;

use App\Support\Pomo\Exception\MsgfmtNotFoundException;

class Compiler
{

    /**
     * msgfmt path resolver command
     * @var string
     */
    private static $command = "which msgfmt";

    function __construct()
    {
        if(!self::checkMsgfmt()) {
            throw new MsgfmtNotFoundException("msgfmt not found. Make sure gettext and it's dependencies are installed.");
        }
    }

    /**
     * Compiles a .po file to it's equivalent .mo file using system libraries
     * @param  string $source The source .po file
     * @return bool
     */
    public function compile($source) {
        $dest = str_replace(".po", ".mo", $source);
        return boolval(exec(
            sprintf("$(%s) -o %s %s", self::$command, $dest, $source)
        ));
    }

    /**
     * Helper to check if msgfmt actually exists
     * @return [type] [description]
     */
    private static function checkMsgfmt() {
        return shell_exec(self::$command);
    }
}
?>