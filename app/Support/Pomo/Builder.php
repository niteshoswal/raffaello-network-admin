<?php

namespace App\Support\Pomo;

/**
 * .po writer
 */
use Geekwright\Po\PoEntry;
use Geekwright\Po\PoFile;
use Geekwright\Po\PoTokens;
use Geekwright\Po\PoHeader;
use Geekwright\Po\Exceptions\FileNotWritableException;

/**
 * .po to .mo compiler
 */
use App\Support\Pomo\Compiler;

use App\Repositories\TranslationRepository;

use Storage;


class Builder {

    protected $file;

    protected $compiler;

    protected $repository;

    public function __construct(TranslationRepository $repository) {
        $this->repository = $repository;
        $this->file = new PoFile;
        $this->compiler = new Compiler;
    }

    public function setEntry($key, $message, $translated) {
        $entry = new PoEntry;
        $entry->set(PoTokens::TRANSLATOR_COMMENTS, sprintf("KEY : %s", $key));
        $entry->set(PoTokens::MESSAGE, $message);
        $entry->set(PoTokens::TRANSLATED, $translated);
        $this->file->addEntry($entry);
    }

    public function setHeaders(array $headers) {
        $header = new PoHeader();
        foreach ($headers as $key => $value) {
            $header->setHeader($key, $value);
        }
        $this->file->setHeaderEntry($header);
    }

    public function dump() {
        $this->file->dumpString();
    }

    public function publish($language) {
        $localeDir = sprintf("locale/%s/LC_MESSAGES/", $this->repository->getLocale($language));
        Storage::makeDirectory($localeDir); //make the messages directory in storage!
        try {
            $finalDestination = storage_path(sprintf("app/%s/domain.po", $localeDir)); //see what I did here?
            $this->file->writePoFile($finalDestination);
            $this->compiler->compile($finalDestination);
            return true;
        } catch(FileNotWritableException $e) {
            echo "Cannot write PO file : ";
            print_r($e->getMessage());
            return false;
        }
        
    }

}

?>