<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Support\Pomo\Builder;
use App\Repositories\TranslationRepository;

class PoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(TranslationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Register the application services.
     *
     * @return void
     */

    public function register()
    {
        $this->app->singleton(Builder::class, function($app) {
            return new Builder($this->repository);
        });
    }
}
