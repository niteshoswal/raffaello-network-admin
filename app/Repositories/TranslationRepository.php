<?php

namespace App\Repositories;

use App\Translation;
use DB;

class TranslationRepository {
    protected $model;

    protected $languages = [
        "english" => "English",
        "portugues" => "Portugues",
        "espanol" => "Espanol",
        "deutsch" => "Deutsch",
        "francais" => "Francais",
        "russian" => "Russian",
        "japanese" => "Japanese",
        "chinese" => "Chinese",
        "arabic" => "Arabic"
    ];

    protected $locales = [
        "english" => "en_US",
        "portugues" => "pt_PT",
        "espanol" => "es_ES",
        "deutsch" => "de_DE",
        "francais" => "fr_FR",
        "russian" => "ru_RU",
        "japanese" => "ja_JP",
        "chinese" => "zh_CN",
        "arabic" => "ar_AE"
    ];

    protected $defaultLanguage = "english";
    
    public function __construct(Translation $translation) {
        $this->model = $translation;
    }

    /**
     * The languages we tend to speak?
     * @return array
     */
    public function getLanguages() {
        return $this->languages;
    }

    /**
     * Get the language index
     * @param  mixed $index The internal language index for a language
     * @return string
     */
    public function getLanguage( $index = false ) {
        if(!$index) $index = $this->defaultLanguage;
        return $this->languages[$index];
    }

    /**
     * Let the world know the language we speak
     * @return string
     */
    public function getDefaultLanguage() {
        return $this->defaultLanguage;
    }

    /**
     * Fetch the world-like locale of a language
     * @param  string $language The language index, internal type
     * @return mixed
     */
    public function getLocale($language) {
        return $this->locales[$language];
    }

    /**
     * Expose the model to the brutal world
     * @return mixed
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * Get translations for a specific language
     * @param  string $language
     * @return mixed
     */
    public function getTranslations( $language = false ) {
        if(!$language) $language = $this->defaultLanguage;
        return $this->model
            ->select(["_t.*", 'translations.*'])
            ->leftJoin(DB::raw(
                sprintf("(SELECT `key` as `translated_key`, `translation` as `translated_text`, `type` as `translated_type`, `revised` as `original_revised`, `automatic` as `original_automatic`, `users`.`name` as `translator` FROM `translations` JOIN `users` ON `users`.`id` = `translations`.`user_id` WHERE `active` = '1' AND `type` = '%s' AND `oc` = '0') as _t", $language)
            ), function($join) {
                $join->on('translations.key', '=', '_t.translated_key');
            })
            ->where(['type' => $this->defaultLanguage, 'active' => '1', 'oc' => '1'])->orderBy('id', 'desc')->get();
    }

    public function getNonEmptyTranslations( $language = false ) {
        if(!$language) $language = $this->defaultLanguage;
        return $this->model
            ->select(["_t.*", 'translations.*'])
            ->leftJoin(DB::raw(
                sprintf("(SELECT `key` as `translated_key`, `translation` as `translated_text`, `type` as `translated_type`, `revised` as `original_revised`, `automatic` as `original_automatic`, `users`.`name` as `translator` FROM `translations` JOIN `users` ON `users`.`id` = `translations`.`user_id` WHERE `active` = '1' AND `type` = '%s' AND `oc` = '0') as _t", $language)
            ), function($join) {
                $join->on('translations.key', '=', '_t.translated_key');
            })
            ->where('translated_text', "!=", "")
            ->where(['type' => $this->defaultLanguage, 'active' => '1', 'oc' => '1'])->orderBy('id', 'desc')->get();
    }

    /**
     * Get previous versions of a language
     * @param  string $key      translation key
     * @param  string $language internal language key
     * @return array            bunch of translations
     */
    public function getPreviousVersions($key, $language) {
        return $this->model
            ->with('user')
            ->where(['type' => $language, 'key' => $key, 'oc' => '0'])
            ->orderBy('id', 'desc')
            ->get()->toArray();
    }

    /**
     * Reset the active translation for a language key
     * @param  string $type internal language index
     * @param  string $key  the text key
     * @return integer      update count
     */
    public function resetActive($type, $key) {
        return $this->update([
            'type' => $type,
            'key' => $key,
            'oc' => '0'
        ], [
            'active' => '0'
        ]);
    }

    /**
     * Create a new translation
     * @param  array $translation  translation attributes
     * @return array               serialized App\Translation
     */
    public function create($translation) {
        return $this->model->create($translation);
    }

    /**
     * Update a translation based on a where clause
     * @param  array $where   seriously, where?
     * @param  array $update  what to update?
     * @return integer        update count
     */
    public function update($where, $update) {
        return $this->model->where($where)->update($update);
    }

}

?>