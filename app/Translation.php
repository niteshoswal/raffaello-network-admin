<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Translation extends Model
{
    /**
     * The database table used by this model.
     * @var string
     */
    protected $table = "translations";

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['key', 'translation', 'type', 'revised', 'automatic', 'user_id', 'active'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
