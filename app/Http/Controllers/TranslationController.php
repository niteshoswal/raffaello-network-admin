<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;

use Illuminate\Http\Request;
use App\Http\Requests\TranslationPost;

use App\Repositories\TranslationRepository;
use Carbon\Carbon;

class TranslationController extends Controller {

    protected $repository;

    protected $middleware = [];

    public function __construct(TranslationRepository $translationRepository) {
        $this->repository = $translationRepository;
    }

    public function index(Request $request) {
        $languages = $this->repository->getLanguages();
        $selectedLanguage = $request->input('language') ? $request->input('language') : $this->repository->getDefaultLanguage();

        $translations = $this->repository->getTranslations($selectedLanguage);

        return view('translations.all', [
            "selectedLanguage" => $selectedLanguage,
            "defaultLanguage" => $this->repository->getDefaultLanguage(),
            "languages" => $this->repository->getLanguages(),
            "translations" => $translations
        ]);
    }

    public function create() {
        return view('translations.create', [
            'languages' => $this->repository->getLanguages(),
            'defaultLanguage' => $this->repository->getDefaultLanguage()
        ]);
    }

    public function store(TranslationPost $request) {
        // create multiple translations if the value's set
        foreach ($request->get('translation') as $translation => $value) {
            if(!empty($value)) { //only if a translation was added..
                $this->repository->resetActive($translation, $request->get('key'));

                $created = $this->repository->create([
                    'key' => $request->get('key'),
                    'translation' => $value,
                    'type' => $translation,
                    'active' => '1',
                    'revised' => '0',
                    'automatic' => '0',
                    'user_id' => Auth::user()->id
                ]);

                if($translation == $this->repository->getDefaultLanguage()) {
                    $this->repository->update(['id' => $created->id], ['oc' => "1"]);
                }
            }
        }
        session()->flash('msg', 'New translation created. Key = ' . $request->get('key'));
        return redirect()->action('TranslationController@index');
    }

    public function fork(Request $request) {
        return response()->json($this->repository->getPreviousVersions($request->get('key'), $request->get('type')));
    }

    public function active(Request $request) {
        $this->repository->resetActive($request->get('type'), $request->get('key'));
        $updated = $this->repository->update([
            'id' => $request->get('id')
        ], [
            'active' => '1'
        ]);
        return response()->json([
            "status" => $updated > 0,
            "request" => $request->all(),

        ]);
    }

    public function revised(Request $request) {
        $updated = $this->repository->update([
            'key' => $request->get('key'),
            'type' => $request->get('type'),
            'active' => '1'
        ], [
            'revised' => '1'
        ]);

        if($updated > 0) {
            return response()->json([
                'status' => true,
                'updated' => $updated
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }

    public function add(Request $request) {
        $this->repository->resetActive($request->get('type'), $request->get('key'));

        $created = $this->repository->create([
            'key' => $request->get('key'),
            'translation' => $request->get('translation'),
            'type' => $request->get('type'),
            'active' => '1',
            'revised' => '0',
            'automatic' => '0',
            'user_id' => Auth::user()->id
        ]);

        if(!$created) {
            return response()->json([
                "status" => false
            ]);
        }

        return response()->json([
            "status" => true,
            "created" => $created->toArray(),
            "name" => Auth::user()->name
        ]);
    }

    public function show( $language ) {
        $builder = app('App\Support\Pomo\Builder');
        $builder->setHeaders([
            'Project-Id-Version' => sprintf("v%s", Carbon::now()->timestamp),
            'Language-Team' => "Nitesh Oswal <nit.oswal@gmail.com>",
            'Report-Msgid-Bugs-To' => "Nitesh Oswal <nit.oswal@gmail.com>",
            'Last-Translator' => sprintf("%s <%s>", Auth::user()->name, Auth::user()->email),
            'X-Generator' => sprintf("raffaello/translator"),
            'Content-Type' => 'text/plain; charset=UTF-8',
            'Content-Transfer-Encoding' => '8bit'
        ]);
        $translations = $this->repository->getNonEmptyTranslations( $language );
        foreach ($translations as $translation) {
            $builder->setEntry($translation->translated_key, $translation->translation, $translation->translated_text);
        }
        $builder->publish($language);
        session()->flash("flash_msg", "Built Locale file with translated texts.");
        return back();
    }

    public function test() {
        $builder = app('App\Support\Pomo\Builder');

        $builder->setHeaders([
            'Project-Id-Version' => sprintf("v%s", Carbon::now()->timestamp),
            'Language-Team' => "Nitesh Oswal <nit.oswal@gmail.com>",
            'Report-Msgid-Bugs-To' => "Nitesh Oswal <nit.oswal@gmail.com>",
            'Last-Translator' => sprintf("%s <%s>", Auth::user()->name, Auth::user()->email),
            'X-Generator' => sprintf("raffaello/translator"),
            'Content-Type' => 'text/plain; charset=UTF-8',
            'Content-Transfer-Encoding' => '8bit'
        ]);

        $builder->setEntry("hello_text", "Hi", "Hola Amigo senör");
        $builder->setEntry("friend_text", "Friend", "Amigo");
        $builder->publish("espanol");
    }
}

?>