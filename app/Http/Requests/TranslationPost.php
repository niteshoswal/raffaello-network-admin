<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Repositories\TranslationRepository;

class TranslationPost extends FormRequest
{
    protected $repository;

    public function __construct(TranslationRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'key' => 'required',
            sprintf("translation.%s", $this->repository->getDefaultLanguage()) => 'required'
        ];
    }

    public function messages() {
        return [
            sprintf("translation.%s.required", $this->repository->getDefaultLanguage()) => sprintf("The %s language translation is actually necessary.", $this->repository->getLanguage())
        ];
    }
}
