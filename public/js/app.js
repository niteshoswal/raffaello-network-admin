// At some point in my life I'd ask myself, do I need a separate file for this?

var refreshTooltips = function() {
    $('[data-toggle=tooltip]').tooltip('destroy');
    $('[data-toggle=tooltip]').tooltip();
}

var translateActions = (function() {
    return {
        changeCheckView: function(el, flag) {
            if(flag) {
                el.removeClass("fa-circle-o").addClass("fa-check-circle bg-success");
            } else {
                el.removeClass("fa-check-circle bg-success").addClass("fa-circle-o");
            }
        },
        addTranslation: function(parent, key, type, translated) {
            var that = this;
            bootbox.prompt({
                title: "Add {" + type + "} translation",
                inputType: 'text',
                value: ((translated == 1) ? $(parent).find(".btn-add-translation").html().trim() : ""),
                callback: function(text) {
                    if(text != null && text.length > 0) {
                        if($(parent).find(".btn-add-translation").html().trim() == text.trim()) {
                            return; //if both the texts are same..
                        }
                        $.post(base_url + '/translations/add', {key: key, type: type, translation: text.trim()}, function(response) {
                            if(response.status) {
                                $(parent).find(".btn-add-translation").data("translated", 1);
                                $(parent).find(".btn-add-translation").html(response.created.translation);
                                var id = $(parent).data('id'),
                                    revisedEl = "#revised-" + id,
                                    automaticEl = "#automatic-" + id;
                                that.changeCheckView($(revisedEl), false);
                                $("[data-parent='" + revisedEl + "']").show();
                                that.changeCheckView($(automaticEl), false);
                                $("[data-parent='" + automaticEl + "']").show();
                                $("#translator-" + id).html(response.name);
                            }
                        });
                    }
                }
            })
        },
        fork: function(key, type) {
            function _list(id, key, type, heading, text, active) {
                return '<a href="#!" onclick="translateActions.setActive(' + id + ', \'' + key + '\', \'' + type + '\');" class="list-group-item ' + ((active == "1") ? "active" : "") + '"><h5 class="list-group-item-heading">' + heading + '</h5><p class="list-group-item-text" style="font-weight: 700;">' + text + '</p></a>';
            }
            $.post(base_url + '/translations/fork', {key: key, type: type}, function(response) {
                var template = '<div class="list-group">';
                if(response.length == 0) {
                    template += '<center class="text-muted"><h3>No previous translations found.</h3></center>';
                }
                response.forEach(function(translation) {
                    template += _list(translation.id, translation.key, translation.type, translation.user.name + "-" + translation.created_at, translation.translation, translation.active);
                });
                template += '</div>';
                bootbox.alert(template);
            });
        },
        setRevised: function(key, type, cb) {
            if(!cb) cb = function() {};
            $.post(base_url + '/translations/revised', {
                key: key,
                type: type
            }, function(response) {
                if(response.status) {
                    cb();
                }
            });
        },
        setActive: function(id, key, type) {
            $.post(base_url + '/translations/active', {
                id: id,
                key: key,
                type: type
            }, function(response) {
                bootbox.hideAll();
            });
        },
        autoTranslate: function(id) {

        },
        save: function(key, type) {
            //not really needed, I guess..
        }
    };
})();


$(function() {

    refreshTooltips();

    $('.table-translations .btn-fork').click(function() {
        translateActions.fork(this.dataset.key, this.dataset.type);
    });

    $('.table-translations .btn-add-translation').click(function() {
        translateActions.addTranslation(this.dataset.parent, this.dataset.key, this.dataset.type, this.dataset.translated);
    });

    $('.table-translations .btn-revised').click(function() {
        var that = this;
        translateActions.setRevised(this.dataset.key, this.dataset.type, function() {
            $(that).tooltip('destroy');
            $(that).hide();
            translateActions.changeCheckView($(that.dataset.parent), true);
        });
    });

});
//Yes